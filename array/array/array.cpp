﻿#include <iostream>
#include <time.h>
using namespace std;


int main()
{
    setlocale(LC_ALL, ("ru"));
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int length = 4;
    int mass[length][length];

    int sum = 0;

    //Заполнение массива
    for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++)
        {
            mass[i][j] = i + j;
        }
    }
    //Вывод массива
    for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++)
        {
            cout << mass[i][j] << "\t";
        }
        cout << endl;
    }
    //сумма элементов строки
    for (int i = 0; i < length; i++) {
        sum += mass[buf.tm_mday % length][i];
    }
    cout << "\nСумма строки с индексом "<< buf.tm_mday % length <<" равна: "<< sum << endl;
    
    return 0;
}


